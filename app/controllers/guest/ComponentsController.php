<?php
namespace Guest;

use Input;
use Redirect;
use Validator;
use View;

use Competency;
use Component;
use Project;

class ComponentsController extends BaseController {

	public function __construct()
	{
		View::share('projects', Project::all());
		
		$competencies = [];
		foreach (Competency::all() as $competency) {
			$competencies[$competency->id] = $competency->title;
		}
		View::share('competencies', $competencies);

		$statusesText = [];
		$statusesText[] = 'Not started';
		$statusesText[] = 'In process';
		$statusesText[] = 'Done';
		View::share('statusesText', $statusesText);

		$statuses = [];
		$statuses[] = 'not-started';
		$statuses[] = 'in-process';
		$statuses[] = 'done';
		View::share('statuses', $statuses);

		parent::__construct();
	}

	/**
	 * Display the specified component.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$component = Component::with(['attachments', 'projects'])->findOrFail($id);

		$this->layout->content = View::make('guest.components.show', compact('component'));
	}

}
