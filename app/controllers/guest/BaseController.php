<?php
namespace Guest;

class BaseController extends \BaseController
{

	protected $layout = 'guest.layouts.master';

	public function __construct()
	{
		// \View::share('navCompetencies', \Competency::all());
		parent::__construct();
	}

}
