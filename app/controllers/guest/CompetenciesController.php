<?php
namespace Guest;

use \Input as Input;
use \Redirect as Redirect;
use \Validator as Validator;
use \View as View;

class CompetenciesController extends BaseController {

	/**
	 * Display a listing of competencies
	 *
	 * @return Response
	 */
	public function index()
	{
		$competencies = \Competency::all();

		$this->layout->content = View::make('guest.competencies.index', compact('competencies'));
	}

}
