<?php
namespace Guest;

class HomeController extends BaseController
{

	public function index()
	{
		$projects = \Project::take(3)->get();
		$this->layout->content = \View::make('guest.home.index', compact('projects'));
	}

}
