<?php
namespace Guest;

use \Input as Input;
use \Redirect as Redirect;
use \Validator as Validator;
use \View as View;

class ProjectsController extends BaseController {

	/**
	 * Display a listing of projects
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = \Project::all();
		$projectsPerRow = 3;
		$imageWidth = 960 / $projectsPerRow - 40;
		$imageHeight = round($imageWidth * 0.5);

		$this->layout->content = View::make('guest.projects.index', compact('projects', 'projectsPerRow', 'imageWidth', 'imageHeight'));
	}

	/**
	 * Display the specified project.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project = \Project::findOrFail($id);

		$this->layout->content = View::make('guest.projects.show', compact('project'));
	}

}
