<?php
namespace Admin;

use Input;
use Redirect;
use Validator;
use View;

use Attachment;
use Competency;
use Component;
use Project;

class AttachmentsController extends BaseController {

	public function __construct()
	{
		View::share('components', Component::all());
		View::share('projects', Project::all());

		$competencies = [];
		foreach (Competency::all() as $competency) {
			$competencies[$competency->id] = $competency->title;
		}
		View::share('competencies', $competencies);
	}

	/**
	 * Display a listing of attachments
	 *
	 * @return Response
	 */
	public function index()
	{
		$attachments = Attachment::paginate(10);

		$this->layout->content = View::make('admin.attachments.index', compact('attachments'));
	}

	/**
	 * Show the form for creating a new attachment
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.attachments.create');
	}

	/**
	 * Store a newly created attachment in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Attachment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$components = isset($data['components']) ? $data['components'] : [];
		$projects = isset($data['projects']) ? $data['projects'] : [];
		unset($data['components'], $data['projects']);

		$attachment = Attachment::create($data);
		
		$this->setComponentsForAttachment($attachment->id, $components);
		$this->setProjectsForAttachment($attachment->id, $projects);

		return Redirect::route('admin.attachments.index');
	}

	/**
	 * Display the specified attachment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$attachment = Attachment::findOrFail($id);

		$this->layout->content = View::make('admin.attachments.show', compact('attachment'));
	}

	/**
	 * Show the form for editing the specified attachment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$attachment = Attachment::find($id);

		$this->layout->content = View::make('admin.attachments.edit', compact('attachment'));
	}

	/**
	 * Update the specified attachment in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$attachment = Attachment::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Attachment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$components = isset($data['components']) ? $data['components'] : [];
		$projects = isset($data['projects']) ? $data['projects'] : [];

		$this->setComponentsForAttachment($id, $components);
		$this->setProjectsForAttachment($id, $projects);
		unset($data['components'], $data['projects']);

		$attachment->update($data);

		return Redirect::route('admin.attachments.index');
	}

	/**
	 * Remove the specified attachment from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Attachment::destroy($id);

		return Redirect::route('admin.attachments.index');
	}

	private function setComponentsForAttachment($attachment_id, $components)
	{
		Attachment::findOrFail($attachment_id)->components()->detach();

		foreach ($components as $component_id) {
			Attachment::findOrFail($attachment_id)->components()->attach($component_id);
		}
	}

	private function setProjectsForAttachment($attachment_id, $projects)
	{
		Attachment::findOrFail($attachment_id)->projects()->detach();

		foreach ($projects as $project_id) {
			Attachment::findOrFail($attachment_id)->projects()->attach($project_id);
		}
	}

}
