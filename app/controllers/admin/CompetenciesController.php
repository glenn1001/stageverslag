<?php
namespace Admin;

use Input;
use Redirect;
use Validator;
use View;

use Competency;

class CompetenciesController extends BaseController {

	/**
	 * Display a listing of competencies
	 *
	 * @return Response
	 */
	public function index()
	{
		$competencies = Competency::all();

		$this->layout->content = View::make('admin.competencies.index', compact('competencies'));
	}

	/**
	 * Show the form for creating a new competency
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.competencies.create');
	}

	/**
	 * Store a newly created competency in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Competency::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Competency::create($data);

		return Redirect::route('admin.competencies.index');
	}

	/**
	 * Display the specified competency.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$competency = Competency::findOrFail($id);

		$this->layout->content = View::make('admin.competencies.show', compact('competency'));
	}

	/**
	 * Show the form for editing the specified competency.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$competency = Competency::find($id);

		$this->layout->content = View::make('admin.competencies.edit', compact('competency'));
	}

	/**
	 * Update the specified competency in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$competency = Competency::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Competency::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$competency->update($data);

		return Redirect::route('admin.competencies.index');
	}

	/**
	 * Remove the specified competency from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Competency::destroy($id);

		return Redirect::route('admin.competencies.index');
	}

}
