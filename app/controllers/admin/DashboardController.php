<?php
namespace Admin;

use View;

class DashboardController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('admin.dashboard.index');
	}

}
