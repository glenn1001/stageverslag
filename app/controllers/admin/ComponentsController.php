<?php
namespace Admin;

use Input;
use Redirect;
use Validator;
use View;

use Competency;
use Component;
use Project;

class ComponentsController extends BaseController {

	public function __construct()
	{
		View::share('projects', Project::all());
		
		$competencies = [];
		foreach (Competency::all() as $competency) {
			$competencies[$competency->id] = $competency->title;
		}
		View::share('competencies', $competencies);

		$statusesText = [];
		$statusesText[] = 'Not started';
		$statusesText[] = 'In process';
		$statusesText[] = 'Done';
		View::share('statusesText', $statusesText);

		$statuses = [];
		$statuses[] = 'not-started';
		$statuses[] = 'in-process';
		$statuses[] = 'done';
		View::share('statuses', $statuses);
	}

	/**
	 * Display a listing of components
	 *
	 * @return Response
	 */
	public function index()
	{
		$components = Component::paginate(30);

		$this->layout->content = View::make('admin.components.index', compact('components'));
	}

	/**
	 * Show the form for creating a new component
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.components.create');
	}

	/**
	 * Store a newly created component in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Component::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$projects = $data['projects'];
		unset($data['projects']);

		$component = Component::create($data);
		
		$this->setProjectsForComponent($component->id, $projects);

		return Redirect::route('admin.components.index');
	}

	/**
	 * Display the specified component.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$component = Component::findOrFail($id);

		$this->layout->content = View::make('admin.components.show', compact('component'));
	}

	/**
	 * Show the form for editing the specified component.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$component = Component::find($id);

		$this->layout->content = View::make('admin.components.edit', compact('component'));
	}

	/**
	 * Update the specified component in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$component = Component::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Component::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['projects'] = Input::get('projects', []);
		$this->setProjectsForComponent($id, $data['projects']);
		unset($data['projects']);

		$component->update($data);

		return Redirect::route('admin.components.index');
	}

	/**
	 * Remove the specified component from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Component::destroy($id);

		return Redirect::route('admin.components.index');
	}

	private function setProjectsForComponent($component_id, $projects)
	{
		Component::findOrFail($component_id)->projects()->detach();

		foreach ($projects as $project_id) {
			Component::findOrFail($component_id)->projects()->attach($project_id);
		}
	}

}
