<?php

class Project extends Model {
	use AttachmentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function components()
	{
		return $this->belongsToMany('Component');
	}

	public function short_body()
	{
		$body = strip_tags($this->body);
		$body = substr($body, 0, 255);
		$body = explode(' ', $body);
		array_pop($body);
		$body = implode(' ', $body) . ' ...';
		return $body;
	}

}
