<?php

trait AttachmentTrait
{
	public function attachments()
	{
		return $this->belongsToMany('Attachment');
	}
}
