<?php

class Attachment extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function components()
	{
		return $this->belongsToMany('Component');
	}

	public function projects()
	{
		return $this->belongsToMany('Project');
	}

}
