<?php

class Component extends Model {
	use AttachmentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function projects()
	{
		return $this->belongsToMany('Project');
	}

	public function competency()
	{
		return $this->belongsTo('Competency');
	}

	public function getShortTitleAttribute()
	{
		$maxLenght = 50;
		$value = $this->attributes['title'];
		if (strlen($value) > $maxLenght) {
			$value = substr($value, 0, $maxLenght);
			$value = explode(' ', $value);
			array_pop($value);
			$value = implode(' ', $value) . ' ...';
		}

		return $value;
	}

}
