@section('main')
	<div class="container">
		<div class="wrapper">
			<div>
				<h1>{{ $project->title }}</h1>
				@if ($project->image !== '')
					<img src="{{ $project->image }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
				@else
					<img src="http://fpoimg.com/920x460?text={{ $project->title }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
				@endif
				<p>{{ $project->body }}</p>
			</div>
			<p>{{ link_to_route('projects.index', 'Terug naar alle projecten', null, array('class'=>'btn btn-lg btn-primary')) }}</p>
		</div>
	</div>
@stop
