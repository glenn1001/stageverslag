@section('main')

	<div class="container">
		<div class="wrapper">
			@if ($projects->count())
				<?php $i = 0; ?>
				@foreach ($projects as $project)
					@if ($i % $projectsPerRow === 0)
						<div class="row">
					@endif
					<div class="column column-1-{{$projectsPerRow}}">
						<a href="/projects/{{$project->id}}">
							<h2>{{ $project->title }}</h2>
							@if ($project->image !== '')
								<img src="{{ $project->image }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
							@else
								<img src="http://fpoimg.com/{{$imageWidth}}x{{$imageHeight}}?text={{ $project->title }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
							@endif
						</a>
						<p>{{ $project->short_body() }}</p>
						{{ link_to_route('projects.show', 'Bekijk project', array($project->id), array('class' => 'btn')) }}
					</div>
					<?php $i++; ?>
					@if ($i % $projectsPerRow === 0)
						</div>
					@endif
				@endforeach
				@if ($i % $projectsPerRow !== 0)
					</div>
				@endif
			@else
				Er zijn geen projecten.
			@endif
		</div>
	</div>

@stop
