@section('main')
	<div class="container banner">
		<div class="wrapper">
			<!-- <img src="/images/banner1.png" alt="Burst in Madrid" title="Burst in Madrid"> -->
			<img src="/images/banner2.png" alt="Kantoor Burst" class="margin" title="Kantoor Burst">
			<!-- <img src="http://fpoimg.com/940x450?text=Banner"> -->
		</div>
	</div>
	<div class="container">
		<div class="wrapper">
			@foreach ($projects as $project)
				<div class="row">
					<div class="column column-2-3">
						@if ($project->image !== '')
							<img src="{{ $project->image }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
						@else
							<img src="http://fpoimg.com/600x300?text={{ $project->title }}" alt="{{ $project->title }}" title="{{ $project->title }}" />
						@endif
					</div>
					<div class="column column-1-3">
						<h2>{{ $project->title }}</h2>
						<p>{{ $project->short_body() }}</p>
						{{ link_to_route('projects.show', 'Bekijk project', array($project->id), array('class' => 'btn')) }}
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop
