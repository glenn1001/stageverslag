<!DOCTYPE html>
<html>
<head>
	<title>Stageverslag</title>
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<header class="container">
		<nav class="wrapper aligncenter">
			<ul>
				<li><a href="{{ URL::route('guest.index') }}">Home</a></li>
				<li><a href="{{ URL::route('competencies.index') }}">Competenties</a></li>
				<li><a href="{{ URL::route('projects.index') }}">Projecten</a></li>
			</ul>
		</nav>
	</header>
	
	@yield('main')

	<footer class="container">
		<div class="wrapper">

		</div>
	</footer>

	<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>