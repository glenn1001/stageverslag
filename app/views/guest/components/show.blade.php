@section('main')

	<div class="container">
		<div class="wrapper">
			<h1>{{ $component->title }}</h1>
			<p>{{ $component->body }}</p>

			@if (count($component->projects) > 0)
				<h2>Toegepast bij</h2>
				@foreach ($component->projects as $project)
					<div>
						<h2>{{ $project->title }}</h2>
						<p>{{ $project->body }}</p>
						{{ link_to_route('projects.show', 'Meer informatie', array($project->id)) }}
					</div>
				@endforeach
			@endif

			<h2>Bijlages</h2>
			@if (count($component->attachments) > 0)
				<ul>
					@foreach ($component->attachments as $attachment)
						<li>{{ HTML::link($attachment->src, $attachment->name, array('title' => $attachment->name, 'target' => '_blank')) }}</li>
					@endforeach
				</ul>
			@else
				<p>Er zijn geen bijlages bij deze competentie.</p>
			@endif
		</div>
	</div>

@stop
