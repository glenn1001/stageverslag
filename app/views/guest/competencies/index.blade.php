@section('main')
	
	<div class="container">
		<div class="wrapper">
			<h1>Competenties</h1>

			@if ($competencies->count())
				@foreach ($competencies as $competency)
					<div>
						<h2>{{ $competency->title }}</h2>
						<ul>
							@foreach ($competency->components as $component)
								<li>{{ link_to_route('components.show', $component->short_title, array($component->id)) }}</li>
							@endforeach
						</ul>
					</div>
				@endforeach
			@endif
		</div>
	</div>

@stop
