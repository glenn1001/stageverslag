@section('main')

<h1>All Projects</h1>

<p>{{ link_to_route('admin.projects.create', 'Add New Project', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($projects->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($projects as $project)
				<tr>
					<td>{{ $project->title }}</td>
                    <td>
                        {{ link_to_route('admin.projects.edit', 'Edit', array($project->id), array('class' => 'btn btn-info')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.projects.destroy', $project->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no projects
@endif

@stop
