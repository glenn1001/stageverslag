@section('main')

<h1>All Attachments</h1>

<p>{{ link_to_route('admin.attachments.create', 'Add New Attachment', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($attachments->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Type</th>
				<th>Src</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($attachments as $attachment)
				<tr>
					<td>{{ $attachment->name }}</td>
					<td>{{ $attachment->type }}</td>
                    <td>
                        {{ HTML::link($attachment->src, 'View', ['target' => '_blank'])}}
                        {{ link_to_route('admin.attachments.edit', 'Edit', array($attachment->id), array('class' => 'btn btn-info')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.attachments.destroy', $attachment->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{{ $attachments->links() }}
@else
	There are no attachments
@endif

@stop
