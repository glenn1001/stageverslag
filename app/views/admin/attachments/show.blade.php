@section('main')

<h1>Show Attachment</h1>

<p>{{ link_to_route('admin.attachments.index', 'Return to All attachments', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Type</th>
				<th>Src</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{ $attachment->name }}</td>
					<td>{{ $attachment->type }}</td>
					<td>{{ $attachment->src }}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.attachments.destroy', $attachment->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.attachments.edit', 'Edit', array($attachment->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
