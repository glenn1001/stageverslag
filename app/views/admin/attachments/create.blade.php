@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Attachment</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'admin.attachments.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('type', 'Type:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('type', Input::old('type'), array('class'=>'form-control', 'placeholder'=>'Type')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('src', 'Src:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('src', Input::old('src'), array('class'=>'form-control', 'placeholder'=>'Src')) }}
            </div>
        </div>

        <h2>Projecten</h2>
        <div>
            @foreach ($projects as $project)
                <div class="form-group">
                    {{ 
                        Form::checkbox(
                            'projects[' . $project->id . ']',
                            $project->id,
                            false,
                            array('class'=>'form-control', 'id'=>'projects[' . $project->id . ']')
                        )
                    }}
                    {{ Form::label('projects[' . $project->id . ']', $project->title, array('class'=>'col-md-2 control-label')) }}
                </div>

            @endforeach
        </div>

        <h2>Competentie onderdelen</h2>
        <div>
            @foreach ($components as $component)
                <div class="form-group">
                    {{ 
                        Form::checkbox(
                            'components[' . $component->id . ']',
                            $component->id,
                            false,
                            array('class'=>'form-control', 'id'=>'components[' . $component->id . ']')
                        )
                    }}
                    {{ Form::label('components[' . $component->id . ']', $competencies[$component->competency_id] . ': ' . $component->title, array('class'=>'col-md-2 control-label')) }}
                </div>

            @endforeach
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

@stop


