<!DOCTYPE html>
<html>
<head>
	<title>Stageverslag</title>
	<link rel="stylesheet" href="/css/style.admin.css">
</head>
<body>
	<header class="container">
		<div class="wrapper">
			<nav>
				<ul>
					<li><a href="{{ URL::route('admin.index') }}">Dashboard</a></li>
					<li><a href="{{ URL::route('admin.competencies.index') }}">Competenties</a></li>
					<li><a href="{{ URL::route('admin.components.index') }}">Competentie onderdelen</a></li>
					<li><a href="{{ URL::route('admin.attachments.index') }}">Bijlages</a></li>
					<li><a href="{{ URL::route('admin.projects.index') }}">Projecten</a></li>
					<li><a href="{{ URL::route('admin.users.index') }}">Gebruikers</a></li>
				</ul>
			</nav>
		</div>
	</header>

	<div class="container">
		<div class="wrapper">
			@yield('main')
		</div>
	</div>

	<footer class="container">
		<div class="wrapper">

		</div>
	</footer>

	<script type="text/javascript" src="/vendor/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/js/app.admin.js"></script>
</body>
</html>