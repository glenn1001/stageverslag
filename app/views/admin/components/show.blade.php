@section('main')

<h1>Show Componennt</h1>

<p>{{ link_to_route('admin.components.index', 'Return to All components', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Competency</th>
				<th>Title</th>
				<th>Body</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{ $competencies[$component->competency_id] }}</td>
					<td>{{ $component->title }}</td>
					<td>{{ $component->body }}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.components.destroy', $component->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.components.edit', 'Edit', array($component->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
