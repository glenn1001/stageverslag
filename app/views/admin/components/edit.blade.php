@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Componennt</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($component, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.components.update', $component->id))) }}

        <div class="form-group">
            {{ Form::label('competency_id', 'Competency:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('competency_id', $competencies, Input::old('competency_id'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('status', 'Status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('status', $statusesText, Input::old('status'), array('class'=>'form-control')) }}
            </div>
        </div>

        <h2>Projecten</h2>
        <div>
            @foreach ($projects as $project)
                <div class="form-group">
                    {{ 
                        Form::checkbox(
                            'projects[' . $project->id . ']',
                            $project->id,
                            $component->projects->find($project->id) === null ? false : true,
                            array('class'=>'form-control', 'id'=>'projects[' . $project->id . ']')
                        )
                    }}
                    {{ Form::label('projects[' . $project->id . ']', $project->title, array('class'=>'col-md-2 control-label')) }}
                </div>

            @endforeach
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.components.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop