@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Componennt</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'admin.components.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('competency_id', 'Competency:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('competency_id', $competencies, Input::old('competency_id'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control', 'placeholder'=>'Body')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('status', 'Status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::select('status', $statusesText, Input::old('status'), array('class'=>'form-control')) }}
            </div>
        </div>

        <h2>Projecten</h2>
        <div>
            @foreach ($projects as $project)
                <div class="form-group">
                    {{ 
                        Form::checkbox(
                            'projects[' . $project->id . ']',
                            $project->id,
                            false,
                            array('class'=>'form-control', 'id'=>'projects[' . $project->id . ']')
                        )
                    }}
                    {{ Form::label('projects[' . $project->id . ']', $project->title, array('class'=>'col-md-2 control-label')) }}
                </div>

            @endforeach
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

@stop


