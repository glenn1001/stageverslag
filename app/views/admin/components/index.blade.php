@section('main')

<h1>All Componennts</h1>

<p>{{ link_to_route('admin.components.create', 'Add New Componennt', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($components->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Competency</th>
				<th>Title</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($components as $component)
				<tr class="{{ $statuses[$component->status] }}">
					<td>{{ $competencies[$component->competency_id] }}</td>
					<td>{{ $component->title }}</td>
                    <td>
                        {{ link_to_route('admin.components.edit', 'Edit', array($component->id), array('class' => 'btn btn-info')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.components.destroy', $component->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{{ $components->links() }}
@else
	There are no components
@endif

@stop
