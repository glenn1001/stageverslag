@section('main')

<h1>All Compettencies</h1>

<p>{{ link_to_route('admin.competencies.create', 'Add New Compettency', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($competencies->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($competencies as $competency)
				<tr>
					<td>{{ $competency->title }}</td>
                    <td>
                        {{ link_to_route('admin.competencies.edit', 'Edit', array($competency->id), array('class' => 'btn btn-info')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.competencies.destroy', $competency->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no competencies
@endif

@stop
