<?php

Route::group(array('namespace' => 'Admin', 'prefix' => 'admin'), function() {
	Route::group(array('before' => 'auth'), function() {

	});
	Route::get('/', array('uses'					=> 'DashboardController@index', 	'as' => 'admin.index'));

	Route::resource('competencies', 				'CompetenciesController');
	Route::resource('components', 					'ComponentsController');
	Route::resource('attachments', 					'AttachmentsController');
	Route::resource('projects', 					'ProjectsController');
	Route::resource('users', 						'UsersController');
});

Route::group(array('namespace' => 'Guest'), function() {
	Route::get('/', array('uses'					=> 'HomeController@index', 			'as' => 'guest.index'));
	Route::resource('competencies', 				'CompetenciesController');
	Route::resource('components', 					'ComponentsController');
	Route::resource('projects', 					'ProjectsController');
});


Route::resource('attachments', 'AttachmentsController');