<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('CompettenciesTableSeeder');
		$this->call('ComponenntsTableSeeder');
		$this->call('ProjectsTableSeeder');
		$this->call('AttachmentsTableSeeder');
	}

}
